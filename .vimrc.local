"*****************************************************************************
"" General Settings "
"*****************************************************************************

" TAB Settings
set tabstop=2
set softtabstop=2
set shiftwidth=2

" Set path separator as /
set shellslash

" Enable mouse
set mouse=a

" Move over wrapped lines
set whichwrap=b,s,h,l,<,>,[,],~

" Use clipboard on Visual mode
set guioptions+=a
set clipboard&
set clipboard=unnamedplus

" Visual mode setting
set virtualedit+=block


"*****************************************************************************
"" Display Settings "
"*****************************************************************************

set relativenumber
set cursorline
set showcmd
set showbreak
set display=lastline
set pumheight=10

" Blink on corresponding brackets
set showmatch
set matchtime=1

" Show invisible characters
set listchars=eol:~,tab:>\ ,extends:<
set listchars=tab:^\ ,trail:~

" Show TAB as ▸-
set list listchars=tab:\▸\-


"*****************************************************************************
"" Visual Settings "
"*****************************************************************************

set background=dark


"*****************************************************************************
"" Search Settings
"*****************************************************************************

" Back to start when search ends
set wrapscan


"*****************************************************************************
"" Mappings
"*****************************************************************************

" Map leader
let mapleader="\<Space>"

" Force Japanese IME Typing
nnoremap あ a
nnoremap い i
nnoremap う u
nnoremap お o
nnoremap っｄ dd
nnoremap っｙ yy

" Move wrapped lines
nnoremap j gj
nnoremap k gk

" Yank until EOL
nnoremap Y y$

" Increment/Decrement numbers
nnoremap + <C-a>
nnoremap - <C-x>

" Save and Quit
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q!<CR>
nnoremap <Leader>wq :wq!<CR>

" Replace
nnoremap <Leader>s :%s/

" Escape with jj
inoremap <silent> jj <Esc>
inoremap <silent> っｊ <Esc>

" Brackets Auto Indent
inoremap {<Enter> {}<Left><CR><ESC><S-o>
inoremap [<Enter> []<Left><CR><ESC><S-o><TAB>
inoremap (<Enter> ()<Left><CR><ESC><S-o>

" Auto Indent Format
map <leader>i gg=<S-g><C-o><C-o>zz

" Release Highlights
nmap <Esc><Esc> :nohlsearch<CR><Esc>
