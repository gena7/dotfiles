#!/bin/bash

# include .common_profile if it exists
if [ -f "$HOME/.common_profile" ]; then
  . "$HOME/.common_profile"
fi

# include .common_env if it exists
if [ -f "$HOME/.common_env" ]; then
  . "$HOME/.common_env"
fi

if [ -n "$BASH_VERSION" ]; then
# include .bashrc if it exists
  if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
  fi
fi
