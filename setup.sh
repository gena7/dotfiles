#!/bin/sh

DOT_FILES=(.vimrc .vimrc.local .common_profile .common_aliases .common_env .gitconfig)
BASH_DOT_FILES=(.bash_profile .bashrc .bash_aliases .bashenv)
ZSH_DOT_FILES=(.zprofile .zshrc .zsh_aliases .zshenv .zlogin .zlogout .zpreztorc)
MAC_DOT_FILES=(.mac_aliases)
UBUNTU_DOT_FILES=(.ubuntu_aliases)

_PID=$$;
_PPID=$(ps -o ppid -p $_PID | tail -n 1);

if ps -p $_PPID | grep -qs bash; then
  DOT_FILES=(${DOT_FILES[@]} ${BASH_DOT_FILES[@]})
elif ps -p $_PPID | grep -qs zsh; then
  DOT_FILES=(${DOT_FILES[@]} ${ZSH_DOT_FILES[@]})
fi

if [ "$(uname)" == 'Darwin' ]; then
  DOT_FILES=(${DOT_FILES[@]} ${MAC_DOT_FILES[@]})
elif [ "$(expr substr $(uname -s) 1 5)" == 'Linux' ]; then
  if [ -e /etc/lsb-release ]; then
    DOT_FILES=(${DOT_FILES[@]} ${UBUNTU_DOT_FILES[@]})
  fi
fi

for file in ${DOT_FILES[@]}
do
  if [ ! -L "$HOME/${file}" ]; then
    if [ -f "./${file}" ]; then
      echo ${file}
      ln -s $HOME/dotfiles/$file $HOME/$file
    else
      echo "WARNING: ${file} does not exist."
    fi
  fi
done
