#!/bin/bash

# include .common_aliases if it exists
if [ -f ~/.common_aliases ]; then
  . ~/.common_aliases
fi

# command cd with pushd
cd() {
  pushd "$@" && ls -a
}
popd() {
  builtin popd && ls -a
}

# Bash dot files
alias vba="vim ~/.bash_aliases"
alias vbr="vim ~/.bashrc"
alias vbp="vim ~/.bash_profile"
alias vbe="vim ~/.bashenv"
alias sbp="source ~/.bash_profile"
