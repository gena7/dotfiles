#!/bin/zsh

: "General Settings" && {
  autoload -Uz compinit && compinit -d ${COMPDUMPFILE} # 補完機能の強化
  setopt correct # 入力しているコマンド名が間違っている場合にもしかして：を出す。
  setopt nobeep # ビープを鳴らさない
  setopt no_tify # バックグラウンドジョブが終了したらすぐに知らせる。
  unsetopt auto_menu # タブによるファイルの順番切り替えをしない
  setopt auto_pushd # cd -[tab]で過去のディレクトリにひとっ飛びできるようにする
  setopt auto_cd # ディレクトリ名を入力するだけでcdできるようにする
  setopt interactive_comments # コマンドラインでも # 以降をコメントと見なす
}

: "History Settings" && {
  HISTSIZE=10000 # メモリに保存される履歴の件数
  SAVEHIST=10000 # 履歴ファイルに保存される履歴の件数
  setopt hist_ignore_dups # 直前と同じコマンドをヒストリに追加しない
  setopt hist_ignore_all_dups # 重複するコマンドは古い法を削除する
  setopt share_history # 異なるウィンドウでコマンドヒストリを共有する
  setopt hist_no_store # historyコマンドは履歴に登録しない
  setopt hist_reduce_blanks # 余分な空白は詰めて記録
  setopt hist_verify # `!!`を実行したときにいきなり実行せずコマンドを見せる
}

: "Plugins" && {
  ### Added by Zplugin's installer
  source "$HOME/.zplugin/bin/zplugin.zsh"
  autoload -Uz _zplugin
  (( ${+_comps} )) && _comps[zplugin]=_zplugin
  ### End of Zplugin installer's chunk

  zplugin light sorin-ionescu/prezto
  zplugin light momo-lab/zsh-abbrev-alias
  zplugin ice wait'!0'; zplugin light b4b4r07/enhancd
  zplugin ice wait'!0'; zplugin light zsh-users/zsh-autosuggestions
  zplugin ice wait'!0'; zplugin light zsh-users/zsh-completions
  zplugin ice wait'!0'; zplugin light zsh-users/zsh-syntax-highlighting
  zplugin ice wait'!0'; zplugin light zdharma/history-search-multi-word

  ### zsh-notify Settings ###
  if [[ $TERM_PROGRAM = 'iTerm.app' || $TERM_PROGRAM = 'Apple_Terminal' ]]; then
    zplugin ice wait'!0'; zplugin light marzocchi/zsh-notify
    export SYS_NOTIFIER="/usr/local/bin/terminal-notifier"
    export NOTIFY_COMMAND_COMPLETE_TIMEOUT=10
  fi
}

: "cd先のディレクトリのファイル一覧を表示する" && {
  [ -z "$ENHANCD_ROOT" ] && function chpwd { ls -A } # enhancdがない場合
  [ -z "$ENHANCD_ROOT" ] || export ENHANCD_HOOK_AFTER_CD="ls -A" # enhancdがあるときはそのHook機構を使う
}

: "sshコマンド補完を~/.ssh/configから行う" && {
  function _ssh { compadd $(fgrep 'Host ' ~/.ssh/*/config | grep -v '*' |  awk '{print $2}' | sort) }
}

: "direnv" && {
  eval "$(direnv hook zsh)"
}

# include .zsh_aliases if it exists
if [ -f ~/.zsh_aliases ]; then
  . ~/.zsh_aliases
fi

